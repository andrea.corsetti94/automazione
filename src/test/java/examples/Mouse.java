package examples;

import org.openqa.selenium.WebElement;

public class Mouse {

    public static void click(WebElement element) {
        if (null == element) {
            System.out.println("Can't click: Null Web Element");
            return;
        }
        element.click();
    }

}
