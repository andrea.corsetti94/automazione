package examples;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FindElements {

    public static WebElement findById(WebDriver webDriver, String id){
        try {
            return webDriver.findElement(By.id(id));
        } catch (NoSuchElementException ex) {
            System.out.println("No such element found by id: " + id);
            return null;
        }
    }

    public static WebElement findByName(WebDriver webDriver, String name) {
        try{
            return webDriver.findElement(By.name(name));
        }
        catch (NoSuchElementException ex){
            System.out.println("No such element found by name: " + name );
            return null;
        }
    }

    public static WebElement findByClassName(WebDriver webDriver, String className) {
        try {
            return webDriver.findElement(By.className(className));
        } catch (NoSuchElementException ex) {
            System.out.println("No such element found by className: " + className);
            return null;
        }
    }

    //Avoid: not descriptive
    public static WebElement findByXPath(WebDriver webDriver, String xPath) {
        try {
            return webDriver.findElement(By.xpath(xPath));
        } catch (NoSuchElementException ex) {
            System.out.println("No such element found by xpath: " + xPath);
            return null;
        }
    }
}
