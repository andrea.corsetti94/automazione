package examples;

import org.openqa.selenium.WebElement;

public class WriteText {

    public static void sendKeys(WebElement element, String text){
        if ( element == null ){
            System.out.println("Can't send keys: Null Web element");
            return;
        }
        element.sendKeys(text);
    }
}
