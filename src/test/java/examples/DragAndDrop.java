package examples;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class DragAndDrop {

    public static void dragAndDrop(WebDriver driver, WebElement origin, WebElement destination){
        Actions actions = new Actions(driver);
        actions.dragAndDrop(origin, destination).build().perform();
    }
}
