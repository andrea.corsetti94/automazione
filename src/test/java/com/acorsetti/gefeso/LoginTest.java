package com.acorsetti.gefeso;

import com.acorsetti.gefeso.driver.DriverManager;
import com.acorsetti.gefeso.page.LoginPage;
import com.acorsetti.gefeso.page.PageConfiguration;
import com.acorsetti.gefeso.profile.Profile;
import com.acorsetti.gefeso.profile.ProfileConfiguration;
import com.acorsetti.gefeso.profile.ProfileTransformer;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.util.List;

import static org.testng.Assert.fail;

public class LoginTest extends BaseTest {

    private WebDriver driver;

    public LoginTest() {
    }

    @BeforeTest
    public void setUp() {

        driver = DriverManager.getWebDriver();

        super.setBasePage( new LoginPage(driver) );

        String chooseRoleUrl = new PageConfiguration().getGefesoChooseRoleUrl();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.urlToBe(chooseRoleUrl));

    }

    @Parameters( { "profile"} )
    @Test
    public void login(String profileConfKey) {

        ProfileConfiguration profileConfiguration = new ProfileConfiguration();
        String profileString = profileConfiguration.getProfileByKey( profileConfKey );
        Profile profile = new ProfileTransformer(profileString).transform();

        int roleCode = profile.getRoleCode();
        String roleDesc = profile.getRoleDesc();
        String structureCode = profile.getStructureCode();

        String tableXPath = "/html/body/div[2]/chooserole/div/table";
        WebDriverWait wait = new WebDriverWait(driver, 5);
        WebElement table = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(tableXPath)));

        List<WebElement> rows = table.findElements(By.tagName("tr"));
        for(WebElement row: rows){
            List<WebElement> cells = row.findElements(By.tagName("td"));

            if ( cells.size() < 3 ) continue; //table header

            int columnIndex = 0;
            String roleCodeString = cells.get(columnIndex++).getText();
            String roleDescString = cells.get(columnIndex++).getText();
            String structureDescString = cells.get(columnIndex).getText();

            int rowRoleCode = 0;
            try{
                rowRoleCode = Integer.parseInt(roleCodeString);
            } catch (NumberFormatException e){
                String error = "Expected: Integer role code. Found: " + roleCodeString;
                logger.error(error, e);
                fail(error);
            }

            if ( roleCode == rowRoleCode && roleDescString.equals(roleDesc)
                    && structureCode.equals(structureDescString) ){
                logger.info("Login Profile found. Logging in as: " + profile.toString());

                //double click
                new Actions(driver).moveToElement(row).doubleClick().perform();
                return;
            }
        }

        fail("No profile found with role: " + roleCode + " - " + roleDesc + " and structure: " + structureCode);
    }

    @AfterTest
    public void checkPostCondition(){
        String bachecaUrl = new PageConfiguration().getBachecaUrl();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.urlToBe(bachecaUrl));

        String currentUrl = driver.getCurrentUrl();
        if ( ! currentUrl.equals(bachecaUrl) ){
            fail("Current URL: " + currentUrl + " differs from expected URL: " + bachecaUrl);
        }

    }

}
