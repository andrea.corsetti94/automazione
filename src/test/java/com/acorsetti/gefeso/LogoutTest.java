package com.acorsetti.gefeso;

import com.acorsetti.gefeso.driver.DriverManager;
import com.acorsetti.gefeso.page.PageConfiguration;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class LogoutTest extends BaseTest {

    private WebDriver driver;
    private static final String LOGOUT_BUTTON_XPATH = "//*[@id=\"barra_navigazione-0\"]/ul/li[2]";
    private static final String LOGOUT_ALERT_XPATH = "/html/body/div[2]/logout/div";

    public LogoutTest(){
    }

    @BeforeTest
    public void setUp(){
        driver = DriverManager.getWebDriver();

        //if not logged in, do it and then logout
        String currentUrl = driver.getCurrentUrl();
        if( ! currentUrl.startsWith(new PageConfiguration().getGefesoHomeUrl()) ){
            IDPCLogin login = new IDPCLogin("amministrativo_ente_pubblico");
            login.executeIDPCLogin();
        }

        //check for logout link to be present
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until( ExpectedConditions.elementToBeClickable(By.xpath(LOGOUT_BUTTON_XPATH) ) );

        //if there is already the logout ALERT fail.
        if ( ! driver.findElements(By.xpath(LOGOUT_ALERT_XPATH)).isEmpty() ){
            Assert.fail("Logout alert should not be already present.");
        }
    }

    @Test
    public void test(){
        WebElement logoutButton = driver.findElement(By.linkText("Esci"));

        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", logoutButton);

    }

    @AfterTest
    public void checkPostCondition(){
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until( ExpectedConditions.presenceOfElementLocated(By.xpath(LOGOUT_ALERT_XPATH) ) );
    }
}
