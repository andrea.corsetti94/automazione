package com.acorsetti.gefeso;

import com.acorsetti.gefeso.page.BasePage;
import org.apache.log4j.Logger;

public abstract class BaseTest {

    protected final Logger logger = Logger.getLogger(this.getClass());

    private BasePage basePage;

    public BaseTest(){
    }

    protected void setBasePage(BasePage basePage){
        this.basePage = basePage;
    }

    protected BasePage getBasePage(){
        return basePage;
    }


}
