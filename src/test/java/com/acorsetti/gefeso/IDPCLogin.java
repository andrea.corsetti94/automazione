package com.acorsetti.gefeso;

import com.acorsetti.gefeso.cns.CNSReadService;
import com.acorsetti.gefeso.driver.DriverManager;

import com.acorsetti.gefeso.page.IDPCAccessPage;
import org.openqa.selenium.*;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


/**
 * IDPCLogin cannot be executed without gefeso role choosing login
 */
public class IDPCLogin extends BaseTest {
    private String profileConfKey;

    public IDPCLogin() {
    }

    public IDPCLogin(String profileConfKey) {
        this.profileConfKey = profileConfKey;
    }

    public void executeIDPCLogin(){
        this.setUp();
        this.loginWithIDPC(this.profileConfKey);
        this.checkPostCondition();
    }

    private WebDriver driver;
    private LoginTest loginTest;

    @BeforeTest
    public void setUp() {
        driver = DriverManager.getWebDriver();

        super.setBasePage( new IDPCAccessPage(driver) );

    }

    @Parameters( { "profile"} )
    @Test
    public void loginWithIDPC(String profileConfKey){

        CNSReadService cnsReadService = new CNSReadService();
        cnsReadService.start();

        driver.get( super.getBasePage().getUrl() );
        driver.findElement(By.id("operation_yes")).click();

        loginTest = new LoginTest();
        loginTest.setUp();
        loginTest.login(profileConfKey);

    }


    @AfterTest
    public void checkPostCondition(){
        loginTest.checkPostCondition();
    }

}
