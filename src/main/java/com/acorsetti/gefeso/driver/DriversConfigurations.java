package com.acorsetti.gefeso.driver;

import com.acorsetti.gefeso.utils.Configuration;


public class DriversConfigurations extends Configuration {

    public DriversConfigurations() {
        super("driver");
    }

    public String getDriverCreatorInstance(){
        String driverPropertyKey = "driverCreatorInstance";
        return super.getApplicationProperty(driverPropertyKey);
    }

    public String getLocalChromeDriverPath() {
        String chromeDriverPathKey = "chromeDriverPath";
        return super.getApplicationProperty(chromeDriverPathKey);
    }

    public String getLocalFirefoxDriverPath() {
        String firefoxDriverPath = "firefoxDriverPath";
        return super.getApplicationProperty(firefoxDriverPath);
    }

}
