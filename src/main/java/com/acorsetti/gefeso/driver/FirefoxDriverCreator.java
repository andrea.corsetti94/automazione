package com.acorsetti.gefeso.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

class FirefoxDriverCreator extends DriverFactory {

    private static final String DRIVER_SYSTEM_PROPERTY = "webdriver.firefox.driver";

    @Override
    public WebDriver createWebDriver() {
        System.setProperty(DRIVER_SYSTEM_PROPERTY, new DriversConfigurations().getLocalFirefoxDriverPath());
        return new FirefoxDriver();
    }
}
