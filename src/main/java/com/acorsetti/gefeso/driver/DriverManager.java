package com.acorsetti.gefeso.driver;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class DriverManager {
    private static final Logger logger = Logger.getLogger(DriverManager.class);

    private static WebDriver webDriver;

    private static WebDriver createWebDriver(){
        String driverCreatorClassname = new DriversConfigurations().getDriverCreatorInstance();
        DriverFactory driverFactory = null;
        try{
            driverFactory = (DriverFactory) Class.forName(driverCreatorClassname).newInstance();
        }

        catch (ClassCastException ex1){
            logger.error("Instance of : " + driverCreatorClassname + " is not a DriverFactory.", ex1);
            System.exit(-1);
        } catch (ClassNotFoundException ex4) {
            logger.error("Class: " + driverCreatorClassname + " not found at startup.", ex4);
            System.exit(-1);
        } catch ( Exception ex3 ){
            logger.error("Exception in reflective method that creates appropriate web driver", ex3);
            System.exit(-1);
        }
        return driverFactory.createWebDriver();
    }

    public static WebDriver getWebDriver(){
        if ( null == webDriver ){
            webDriver = createWebDriver();
        }
        return webDriver;
    }
}
