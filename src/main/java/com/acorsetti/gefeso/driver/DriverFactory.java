package com.acorsetti.gefeso.driver;

import org.openqa.selenium.WebDriver;

abstract class DriverFactory {
    abstract WebDriver createWebDriver();
}
