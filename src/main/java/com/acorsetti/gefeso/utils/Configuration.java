package com.acorsetti.gefeso.utils;

import org.apache.log4j.Logger;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;

public class Configuration {
    private static final Logger logger = Logger.getLogger(Configuration.class);

    private ResourceBundle applicationResourceBundle;

    public Configuration(String propertyFileName){
        this.loadPropertyBundle("configuration/" + propertyFileName);
    }

    protected final String getApplicationProperty(String key){
        String value = "";
        try{
            value = this.applicationResourceBundle.getString(key);
        }
        catch (Exception e){
            logger.error("No Such Property with key: " + key + " defined. Empty property returned.", e);
        }
        return value;
    }

    protected final Set<String> keys(){
        return this.applicationResourceBundle.keySet();
    }

    private void loadPropertyBundle(String propertyFilename){
        try{
            this.applicationResourceBundle = ResourceBundle.getBundle(propertyFilename);
        }
        catch (MissingResourceException e){
            logger.error("Missing resource with filename: " + propertyFilename, e);
        }
    }
}
