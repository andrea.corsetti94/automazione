package com.acorsetti.gefeso.cns;

import org.apache.log4j.Logger;

import java.awt.*;
import java.awt.event.KeyEvent;

public class CNSReadService extends Thread {
    private static final Logger logger = Logger.getLogger(CNSReadService.class);

    private final String cnsPin;
    private int waitCertificatePopupMs;
    private int waitCnsPinPopupMs;

    private Robot robot;

    public CNSReadService() {
        CNSConfiguration configuration = new CNSConfiguration();
        this.cnsPin = configuration.getCnsPin();

        try{
            robot = new Robot();
        }
        catch (AWTException e){
            logger.error("Robot creation failed. Cannot read CNS from SO",e);
        }

        String waitCertificatePopupMsString = configuration.getWaitCertificatePopupTime();
        String waitCnsPinPopupMsString = configuration.getWaitCnsPinPopupTime();
        try{
            waitCertificatePopupMs = Integer.parseInt( waitCertificatePopupMsString );
            waitCnsPinPopupMs = Integer.parseInt( waitCnsPinPopupMsString );
        }
        catch ( NumberFormatException e ){
            logger.error("Error converting to int the following string read by configuration: " +
                    waitCertificatePopupMsString + " and: " + waitCnsPinPopupMsString, e);
        }
    }

    @Override
    public void run() {
        if ( robot == null ) return;

        robot.delay(this.waitCertificatePopupMs);

        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);

        logger.debug("Pressed Enter to close Certificate Popup");

        robot.delay(this.waitCnsPinPopupMs);

        for (char c : cnsPin.toCharArray()) {
            int keyCode = KeyEvent.getExtendedKeyCodeForChar(c);
            robot.keyPress(keyCode);
            robot.delay(100);
            robot.keyRelease(keyCode);
            robot.delay(100);
        }

        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);

        logger.debug("Inserted CNS Pin and pressed Enter to close Pin Popup");
    }
}
