package com.acorsetti.gefeso.cns;

import com.acorsetti.gefeso.utils.Configuration;

public class CNSConfiguration extends Configuration {

    public CNSConfiguration() {
        super("cns");
    }

    public String getCnsPin(){
        String key = "cnsPin";
        return super.getApplicationProperty(key);
    }

    public String getWaitCertificatePopupTime(){
        String key = "waitCertificatePopupTimeMs";
        return super.getApplicationProperty(key);
    }

    public String getWaitCnsPinPopupTime(){
        String key = "waitCnsPinPopupTimeMs";
        return super.getApplicationProperty(key);
    }
}
