package com.acorsetti.gefeso.profile;

public class Profile {
    private int roleCode;
    private String roleDesc;
    private String structureCode;

    public Profile(int roleCode, String roleDesc, String structureCode) {
        this.roleCode = roleCode;
        this.roleDesc = roleDesc;
        this.structureCode = structureCode;
    }

    public int getRoleCode() {
        return roleCode;
    }

    public String getRoleDesc() {
        return roleDesc;
    }

    public String getStructureCode() {
        return structureCode;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "roleCode=" + roleCode +
                ", roleDesc='" + roleDesc + '\'' +
                ", structureCode='" + structureCode + '\'' +
                '}';
    }
}
