package com.acorsetti.gefeso.profile;

import com.acorsetti.gefeso.utils.Configuration;

import java.util.HashMap;
import java.util.Map;

public class ProfileConfiguration extends Configuration {

    public ProfileConfiguration(){
        super("profiles");
    }

    public Map<String, String> getProfilesConfiguration(){
        Map<String,String> profilesConfiguration = new HashMap<>();

        super.keys().forEach( key -> {
            profilesConfiguration.put(key, super.getApplicationProperty(key));
        });

        return profilesConfiguration;
    }

    public String getAmministrativoEntePubblico(){
        String key = "amministrativo_ente_pubblico";
        return super.getApplicationProperty(key);
    }

    public String getAmministrativoRaggruppamento(){
        String key = "amministrativo_raggruppamento";
        return super.getApplicationProperty(key);
    }

    public String getAmministrativoLispa(){
        String key = "amministratore_lispa";
        return super.getApplicationProperty(key);
    }

    public String getProfileByKey(String key){
        return super.getApplicationProperty(key);
    }
}
