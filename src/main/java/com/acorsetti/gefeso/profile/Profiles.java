package com.acorsetti.gefeso.profile;

public enum Profiles {

    AMM_ENTE_PUBBLICO("amministrativo_ente_pubblico"),
    AMM_ENTE_RAGGRUPPAMENTO("amministrativo_raggruppamento"),
    AMM_LISPA("amministratore_lispa");

    private String confKey;

    Profiles(String confKey){
        this.confKey = confKey;
    }

    public String getConfKey(){ return this.confKey; }
}
