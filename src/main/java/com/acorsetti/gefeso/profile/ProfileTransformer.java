package com.acorsetti.gefeso.profile;

import org.apache.log4j.Logger;

import java.util.Arrays;

public class ProfileTransformer {
    private static final Logger logger = Logger.getLogger(ProfileTransformer.class);

    private String profileString;

    public ProfileTransformer(String profileString) {
        this.profileString = profileString;
    }

    public Profile transform(){
        if ( this.profileString == null ){
            logger.error("Profile String obtained through configuration is null: returning empty profile.");
            return new Profile(0,"","");
        }

        String[] properties = this.profileString.split(",");
        try{
            int roleCode = Integer.parseInt(properties[0]);
            return new Profile(roleCode, properties[1].trim(), properties[2].trim());
        }
        catch (NumberFormatException e){
            logger.error("Profile role code is not an integer. Expected: int, found: " + properties[0],e);
        }
        catch (ArrayIndexOutOfBoundsException e){
            logger.error("Properties configuration value has less than 3 fields. Value found: "
                    + Arrays.toString(properties),e);
        }
        catch (NullPointerException e){
            logger.error("Properties configuration value has null fields. Value found: "
                    + Arrays.toString(properties),e);
        }
        logger.error("Returning empty profile.");

        return new Profile(0,"","");

    }
}
