package com.acorsetti.gefeso.page;

import com.acorsetti.gefeso.utils.Configuration;

public class PageConfiguration extends Configuration {

    public PageConfiguration() {
        super("page");
    }

    public String getIdpcGefesoUrl(){
        String key = "idpc";
        return super.getApplicationProperty(key);
    }

    public String getGefesoChooseRoleUrl(){
        String key = "gefesoChooseRoleUrl";
        return super.getApplicationProperty(key);
    }

    public String getGefesoHomeUrl(){
        String key = "homeGefesoUrl";
        return super.getApplicationProperty(key);
    }

    public String getBachecaUrl(){
        String key = "bachecaUrl";
        return super.getApplicationProperty(key);
    }

}
