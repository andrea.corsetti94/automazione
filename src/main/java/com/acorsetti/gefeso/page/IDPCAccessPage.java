package com.acorsetti.gefeso.page;

import org.openqa.selenium.WebDriver;

public class IDPCAccessPage extends BasePage {

    private String url;

    public IDPCAccessPage(WebDriver webDriver) {
        super(webDriver);
        this.url = new PageConfiguration().getIdpcGefesoUrl();
    }

    @Override
    public String getUrl() {
        return this.url;
    }
}
