package com.acorsetti.gefeso.page;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class BasePage {

    private final WebDriver webDriver;
    private static final Logger logger = Logger.getLogger(BasePage.class);

    public BasePage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public WebDriver getWebDriver(){
        return this.webDriver;
    }

    public WebElement findById(WebDriver webDriver, String id){
        try {
            return webDriver.findElement(By.id(id));
        } catch (NoSuchElementException ex) {
            logger.error("No such element found by id: " + id, ex);
            return null;
        }
    }

    public WebElement findByName(WebDriver webDriver, String name) {
        try{
            return webDriver.findElement(By.name(name));
        }
        catch (NoSuchElementException ex){
            logger.error("No such element found by id: " + name, ex);
            return null;
        }
    }

    public WebElement findByClassName(WebDriver webDriver, String className) {
        try {
            return webDriver.findElement(By.className(className));
        } catch (NoSuchElementException ex) {
            logger.error("No such element found by id: " + className, ex);
            return null;
        }
    }

    //Avoid: not descriptive
    public WebElement findByXPath(WebDriver webDriver, String xPath) {
        try {
            return webDriver.findElement(By.xpath(xPath));
        } catch (NoSuchElementException ex) {
            logger.error("No such element found by id: " + xPath, ex);
            return null;
        }
    }

    public abstract String getUrl();
}
