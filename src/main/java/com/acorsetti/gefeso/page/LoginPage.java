package com.acorsetti.gefeso.page;

import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage{

    private final String url;

    public LoginPage(WebDriver webDriver) {
        super(webDriver);
        this.url = new PageConfiguration().getGefesoChooseRoleUrl();
    }

    @Override
    public String getUrl() {
        return this.url;
    }
}
